# - Config file for the Bethloch package

get_filename_component(Bethy_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
include(CMakeFindDependencyMacro)

find_dependency(ZCross REQUIRED)
find_dependency(Boost 1.36 REQUIRED )
# find_dependency(OpenMP REQUIRED)

if(NOT TARGET DFPE::bethy)
    include("${Bethy_CMAKE_DIR}/BethyTargets.cmake")
endif()
