#include <utility>

#include "qtydef/QtyDefinitions.hpp"
#include "bethy/BethyTypes.hpp"
#include "zcross.hpp"
namespace dfpe
{




	class Bethy
	{
		public:

        explicit Bethy(GasMixture mixture): mixture(std::move(mixture)) {};

        QtySiEnergyPerDistance getMeanEnergyLoss(const ParticleSpecie& bullet, const QtySiMomentum& bulletMomentum)   const;
        QtySiEnergyPerDistance getMeanEnergyLoss(const ParticleSpecie& bullet, const QtySiEnergy&   bulletEnergy)   const;
        QtySiEnergyPerDistance getMeanEnergyLoss(const ParticleSpecie& bullet, const QtySiVelocity& bulletVelocity) const;

        QtySiWavenumber getMeanInteractions(const ParticleSpecie& bullet, const QtySiMomentum& bulletMomentum) const;
        QtySiWavenumber getMeanInteractions(const ParticleSpecie& bullet, const QtySiEnergy&   bulletEnergy) const;
        QtySiWavenumber getMeanInteractions(const ParticleSpecie& bullet, const QtySiVelocity& bulletVelocity) const;

        QtySiEnergy getFirstIonizationEnergy(const Molecule& molecule) const;
        QtySiEnergy getFirstIonizationEnergy(const Element& element)   const;

        QtySiEnergy getMeanIonizationEnergy(const Molecule& molecule) const;
        QtySiEnergy getMeanIonizationEnergy(const Element& element)   const;

        QtySiEnergy getMeanExcitationEnergy(const Molecule& molecule) const;
        QtySiEnergy getMeanExcitationEnergy(const Element& element)   const;
    protected:


        unsigned int getMoleculeComponentZ(const Molecule& component) const;
        unsigned int getMoleculeComponentZ(const MoleculeComponent& component) const;

        QtySiEnergyPerDistance getComponentEnergyLoss(const Molecule& molecule, int bulletChargeNumber, const QtySiVelocity& bulletVelocity) const;



    protected:
        const GasMixture mixture;


    public:
        static std::map<int,QtySiEnergy> meanExcitationEnergies;
        static std::map<int,std::pair<QtySiEnergy,QtySiEnergy>> elementIonizationEnergies;
    };
	
}
