#pragma once

namespace dfpe
{
    /// derived dimension for electric field in SI units : M L T⁻²
    typedef boost::units::make_dimension_list<boost::mpl::list<
        boost::units::dim<boost::units::length_base_dimension,			boost::units::static_rational<1>>,
        boost::units::dim<boost::units::mass_base_dimension,      		boost::units::static_rational<1>>,
        boost::units::dim<boost::units::time_base_dimension,      		boost::units::static_rational<-2>>
    >>::type     energy_per_distance_dimension;

    /// derived dimension for electric field in SI units : M T⁻²
    typedef boost::units::make_dimension_list<boost::mpl::list<
        boost::units::dim<boost::units::mass_base_dimension,      		boost::units::static_rational<1>>,
        boost::units::dim<boost::units::time_base_dimension,      		boost::units::static_rational<-2>>
    >>::type     energy_per_area_dimension;

    /// derived dimension for electric field in SI units : M L⁻¹ T⁻²
    typedef boost::units::make_dimension_list<boost::mpl::list<
        boost::units::dim<boost::units::length_base_dimension,			boost::units::static_rational<-1>>,
        boost::units::dim<boost::units::mass_base_dimension,      		boost::units::static_rational<1>>,
        boost::units::dim<boost::units::time_base_dimension,      		boost::units::static_rational<-2>>
    >>::type     energy_per_volume_dimension;

    typedef boost::units::unit<energy_per_distance_dimension, boost::units::si::system> si_energy_per_distance;
    typedef boost::units::unit<energy_per_area_dimension,     boost::units::si::system> si_energy_per_area;
    typedef boost::units::unit<energy_per_volume_dimension,   boost::units::si::system> si_energy_per_volume;

    typedef boost::units::quantity<si_energy_per_distance> QtySiEnergyPerDistance;
    typedef boost::units::quantity<si_energy_per_area>     QtySiEnergyPerArea;
    typedef boost::units::quantity<si_energy_per_volume>   QtySiEnergyPerVolume;
}