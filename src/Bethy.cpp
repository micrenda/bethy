#include "bethy/Bethy.hpp"
#include <boost/units/systems/si/codata/physico-chemical_constants.hpp>
#include <boost/units/systems/si/codata/electron_constants.hpp>
#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>
#include <boost/units/systems/si/codata/universal_constants.hpp>
#include <boost/units/cmath.hpp>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
//TODO: delete the next
#include <boost/units/systems/si/prefixes.hpp>
using namespace std;
using namespace dfpe;
using namespace boost::units;
using namespace boost::units::si::constants;


QtySiEnergy Bethy::getMeanExcitationEnergy(const Molecule& molecule) const
{
    // Uning Bragg composiiton rule as described here:
    //  https://doi.org/10.1093/jicru/os9.1.36

    using namespace boost::accumulators;

    QtySiEnergy     electronvolt = 1. * si::volt * codata::e;

    accumulator_set<double, features<tag::mean>, double> acc;

    size_t moleculeSize = molecule.getSize();
    for (size_t i = 0; i < moleculeSize; i++)
    {
        const MoleculeComponent& component = molecule.getComponent(i);

        if (component.isElement())
        {
            const Element& element = component.getElement();
            acc(log(getMeanExcitationEnergy(element) / electronvolt), weight = element.getZ() * (double) molecule.getQuantity(i));
        }
        else if (component.isMolecule())
        {
            const Molecule& submolecule = component.getMolecule();
            acc(log(getMeanExcitationEnergy(submolecule) / electronvolt), weight = getMoleculeComponentZ(submolecule) * (double) molecule.getQuantity(i));
        }
        else
            assert(false);
    }
    return exp(mean(acc)) * electronvolt;
}

QtySiEnergy Bethy::getMeanExcitationEnergy(const Element& element) const
{
    // Mean excitation potential taken from http://physics.nist.gov/PhysRefData/XrayMassCoef/tab1.html
    // or (10 eV * Z) if no value is found.

    auto it = meanExcitationEnergies.find(element.getZ());
    if (it != meanExcitationEnergies.end())
        return it->second;
    else
        return 10. * si::volt * codata::e * (double) element.getZ();
}

unsigned int Bethy::getMoleculeComponentZ(const Molecule& molecule) const
{
    unsigned int result = 0;

    size_t moleculeSize = molecule.getSize();

    for (size_t i = 0; i < moleculeSize; i++)
        result += getMoleculeComponentZ(molecule.getComponent(i)) * molecule.getQuantity(i);

    return result;
}
unsigned int Bethy::getMoleculeComponentZ(const MoleculeComponent& component) const
{
    if (component.isElement())
        return component.getElement().getZ();
    else if (component.isMolecule())
        return getMoleculeComponentZ(component.getMolecule());
    else
        return 0;
}

QtySiEnergyPerDistance Bethy::getMeanEnergyLoss(const ParticleSpecie& bullet, const QtySiMomentum& bulletMomentum)   const
{
    // We use: E² = (pc)² + (mc²)²
    QtySiEnergy bulletEnergy = sqrt(bulletMomentum * bulletMomentum * codata::c * codata::c + bullet.getMass() * bullet.getMass() * codata::c * codata::c * codata::c * codata::c);
    return getMeanEnergyLoss(bullet, bulletEnergy);
}

QtySiEnergyPerDistance Bethy::getMeanEnergyLoss(const ParticleSpecie& bullet, const QtySiEnergy& bulletEnergy) const
{
    using namespace si::constants;
    QtySiDimensionless gamma = bulletEnergy / (bullet.getMass() * codata::c * codata::c) + 1.;
    QtySiDimensionless beta  = sqrt(1. - 1. / (gamma * gamma));
    QtySiVelocity bulletVelocity = codata::c * beta;
    return getMeanEnergyLoss(bullet, bulletVelocity);
}

QtySiEnergyPerDistance Bethy::getComponentEnergyLoss(const Molecule& molecule, int bulletChargeNumber, const QtySiVelocity& bulletVelocity) const
{
    // electron density
    QtySiDensity       electronDensity         = (double) getMoleculeComponentZ(molecule) * mixture.getComponentDensity(molecule);
    // vacuum permittivity
    QtySiDimensionless betaSquared             = bulletVelocity / codata::c * bulletVelocity / codata::c;
    QtySiDimensionless gammaSquared            = 1. / (1. - betaSquared);

    if (betaSquared * gammaSquared < 0.1 * 0.1)
        throw std::runtime_error("Electronic binding energy are significand and shell correction are needed for βγ < 0.1 (actual value: " + to_string((double) sqrt(betaSquared * gammaSquared)) + ")");

    if (betaSquared * gammaSquared > 100 * 100)
        throw std::runtime_error("Density corrections should be applied for βγ > 100 (actual value: " + to_string((double) sqrt(betaSquared * gammaSquared)) + ")");

    auto               p1 = 4. * M_PI / (codata::m_e * codata::c * codata::c);
    QtySiDensity       p2 = electronDensity * (double ) (bulletChargeNumber * bulletChargeNumber) / betaSquared;
    auto               p3 = codata::e * codata::e / (4. * M_PI * codata::epsilon_0);
    QtySiDimensionless p4 = log(2. * codata::m_e * codata::c * codata::c * betaSquared * gammaSquared / getMeanExcitationEnergy(molecule)) - betaSquared;

    return -p1 * p2 * p3 * p3 * p4;
}

QtySiEnergyPerDistance Bethy::getMeanEnergyLoss(const ParticleSpecie& bullet, const QtySiVelocity& bulletVelocity) const
{
    QtySiEnergyPerDistance result;
    for (const Molecule& molecule: mixture.getComponents())
        result += getComponentEnergyLoss(molecule, bullet.getIonization(), bulletVelocity);
    return result;
}


QtySiWavenumber Bethy::getMeanInteractions(const ParticleSpecie& bullet, const QtySiMomentum& bulletMomentum) const
{
    // We use: E² = (pc)² + (mc²)²
    QtySiEnergy bulletEnergy = sqrt(bulletMomentum * bulletMomentum * codata::c * codata::c + bullet.getMass() * bullet.getMass() * codata::c * codata::c * codata::c * codata::c);
    return getMeanInteractions(bullet, -bulletEnergy);
}

QtySiWavenumber Bethy::getMeanInteractions(const ParticleSpecie& bullet, const QtySiEnergy& bulletEnergy) const
{
    using namespace si::constants;
    QtySiDimensionless a = 1. / (bulletEnergy / (bullet.getMass() * codata::c * codata::c) + 1.);
    QtySiVelocity bulletVelocity = codata::c * sqrt(1. - a * a);
    return getMeanInteractions(bullet, bulletVelocity);
}

QtySiWavenumber Bethy::getMeanInteractions(const ParticleSpecie& bullet, const QtySiVelocity& bulletVelocity) const
{
    using namespace boost::accumulators;

    QtySiWavenumber result;
    for (const Molecule& molecule: mixture.getComponents())
    {
        result += getComponentEnergyLoss(molecule, bullet.getIonization(), bulletVelocity) / getMeanIonizationEnergy(molecule);
    }

    return result;
}


QtySiEnergy Bethy::getFirstIonizationEnergy(const Element& element) const
{
    auto it = elementIonizationEnergies.find(element.getZ());

    if (it != elementIonizationEnergies.end())
        return it->second.first;
    else // Using an approximation
        return 32. * si::volt * codata::e;
}

/**
 * Estimate the molecule first ionization energy using a weighted mean over the element mass
 *
 * @param molecule
 * @return
 */
QtySiEnergy Bethy::getFirstIonizationEnergy(const Molecule& molecule) const
{
    using namespace boost::accumulators;

    QtySiEnergy     electronvolt = 1. * si::volt * codata::e;
    size_t moleculeSize = molecule.getSize();

    accumulator_set<double, features<tag::mean>, double> acc;
    for (size_t i = 0; i < moleculeSize; i ++)
    {
        const MoleculeComponent& component = molecule.getComponent(i);
        if (component.isMolecule())
        {
            const Molecule& submolecule = component.getMolecule();
            acc(getFirstIonizationEnergy(submolecule) / electronvolt, weight=submolecule.getMass() * (double) molecule.getQuantity(i) / codata::m_u);
        }
        else if (component.isElement())
        {
            const Element& element = component.getElement();
            acc(getFirstIonizationEnergy(element) / electronvolt, weight=element.getMass() * (double) molecule.getQuantity(i) / codata::m_u);
        }
        else
            assert(false);
    }
   return mean(acc) * electronvolt;
}


QtySiEnergy Bethy::getMeanIonizationEnergy(const Molecule& molecule) const
{
    // TODO: find a better value for this
    return 32. * si::volt * codata::e;
}

QtySiEnergy Bethy::getMeanIonizationEnergy(const Element& element)   const
{
    // TODO: find a better value for this
    return 32. * si::volt * codata::e;
}


std::map<int,QtySiEnergy> dfpe::Bethy::meanExcitationEnergies = {
        {1, 19.2 * si::volt * codata::e},
        {2, 41.8 * si::volt * codata::e},
        {3, 40 * si::volt * codata::e},
        {4, 63.7 * si::volt * codata::e},
        {5, 76 * si::volt * codata::e},
        {6, 78 * si::volt * codata::e},
        {7, 82 * si::volt * codata::e},
        {8, 95 * si::volt * codata::e},
        {9, 115 * si::volt * codata::e},
        {10, 137 * si::volt * codata::e},
        {11, 149 * si::volt * codata::e},
        {12, 156 * si::volt * codata::e},
        {13, 166 * si::volt * codata::e},
        {14, 173 * si::volt * codata::e},
        {15, 173 * si::volt * codata::e},
        {16, 180 * si::volt * codata::e},
        {17, 174 * si::volt * codata::e},
        {18, 188 * si::volt * codata::e},
        {19, 190 * si::volt * codata::e},
        {20, 191 * si::volt * codata::e},
        {21, 216 * si::volt * codata::e},
        {22, 233 * si::volt * codata::e},
        {23, 245 * si::volt * codata::e},
        {24, 257 * si::volt * codata::e},
        {25, 272 * si::volt * codata::e},
        {26, 286 * si::volt * codata::e},
        {27, 297 * si::volt * codata::e},
        {28, 311 * si::volt * codata::e},
        {29, 322 * si::volt * codata::e},
        {30, 330 * si::volt * codata::e},
        {31, 334 * si::volt * codata::e},
        {32, 350 * si::volt * codata::e},
        {33, 347 * si::volt * codata::e},
        {34, 348 * si::volt * codata::e},
        {35, 343 * si::volt * codata::e},
        {36, 352 * si::volt * codata::e},
        {37, 363 * si::volt * codata::e},
        {38, 366 * si::volt * codata::e},
        {39, 379 * si::volt * codata::e},
        {40, 393 * si::volt * codata::e},
        {41, 417 * si::volt * codata::e},
        {42, 424 * si::volt * codata::e},
        {43, 428 * si::volt * codata::e},
        {44, 441 * si::volt * codata::e},
        {45, 449 * si::volt * codata::e},
        {46, 470 * si::volt * codata::e},
        {47, 470 * si::volt * codata::e},
        {48, 469 * si::volt * codata::e},
        {49, 488 * si::volt * codata::e},
        {50, 488 * si::volt * codata::e},
        {51, 487 * si::volt * codata::e},
        {52, 485 * si::volt * codata::e},
        {53, 491 * si::volt * codata::e},
        {54, 482 * si::volt * codata::e},
        {55, 488 * si::volt * codata::e},
        {56, 491 * si::volt * codata::e},
        {57, 501 * si::volt * codata::e},
        {58, 523 * si::volt * codata::e},
        {59, 535 * si::volt * codata::e},
        {60, 546 * si::volt * codata::e},
        {61, 560 * si::volt * codata::e},
        {62, 574 * si::volt * codata::e},
        {63, 580 * si::volt * codata::e},
        {64, 591 * si::volt * codata::e},
        {65, 614 * si::volt * codata::e},
        {66, 628 * si::volt * codata::e},
        {67, 650 * si::volt * codata::e},
        {68, 658 * si::volt * codata::e},
        {69, 674 * si::volt * codata::e},
        {70, 684 * si::volt * codata::e},
        {71, 694 * si::volt * codata::e},
        {72, 705 * si::volt * codata::e},
        {73, 718 * si::volt * codata::e},
        {74, 727 * si::volt * codata::e},
        {75, 736 * si::volt * codata::e},
        {76, 746 * si::volt * codata::e},
        {77, 757 * si::volt * codata::e},
        {78, 790 * si::volt * codata::e},
        {79, 790 * si::volt * codata::e},
        {80, 800 * si::volt * codata::e},
        {81, 810 * si::volt * codata::e},
        {82, 823 * si::volt * codata::e},
        {83, 823 * si::volt * codata::e},
        {84, 830 * si::volt * codata::e},
        {85, 825 * si::volt * codata::e},
        {86, 794 * si::volt * codata::e},
        {87, 827 * si::volt * codata::e},
        {88, 826 * si::volt * codata::e},
        {89, 841 * si::volt * codata::e},
        {90, 847 * si::volt * codata::e},
        {91, 878 * si::volt * codata::e},
        {92, 890 * si::volt * codata::e},
};


// Kramida, A., Ralchenko, Yu., Reader, J., and NIST ASD Team (2020).
// NIST Atomic Spectra Database (ver. 5.8)
// Available: https://physics.nist.gov/asd [2021, October 16].
// National Institute of Standards and Technology, Gaithersburg, MD.
// DOI: https://doi.org/10.18434/T4W30F

std::map<int,std::pair<QtySiEnergy,QtySiEnergy>> dfpe::Bethy::elementIonizationEnergies = {
        {1,   {13.598434599702 * si::volt * codata::e, 13.598434599702 * si::volt * codata::e}},
        {2,   {24.587389011 * si::volt * codata::e,    24.587389011 * si::volt * codata::e}},
        {3,   {5.391714996 * si::volt * codata::e,     5.391714996 * si::volt * codata::e}},
        {4,   {9.322699 * si::volt * codata::e,        9.322699 * si::volt * codata::e}},
        {5,   {8.298019 * si::volt * codata::e,        8.298019 * si::volt * codata::e}},
        {6,   {11.260288 * si::volt * codata::e,       11.260288 * si::volt * codata::e}},
        {7,   {14.53413 * si::volt * codata::e,        14.53413 * si::volt * codata::e}},
        {8,   {13.618055 * si::volt * codata::e,       13.618055 * si::volt * codata::e}},
        {9,   {17.42282 * si::volt * codata::e,        17.42282 * si::volt * codata::e}},
        {10,  {21.564541 * si::volt * codata::e,       21.564541 * si::volt * codata::e}},
        {11,  {5.13907696 * si::volt * codata::e,      5.13907696 * si::volt * codata::e}},
        {12,  {7.646236 * si::volt * codata::e,        7.646236 * si::volt * codata::e}},
        {13,  {5.985769 * si::volt * codata::e,        5.985769 * si::volt * codata::e}},
        {14,  {8.15168 * si::volt * codata::e,         8.15168 * si::volt * codata::e}},
        {15,  {10.486686 * si::volt * codata::e,       10.486686 * si::volt * codata::e}},
        {16,  {10.36001 * si::volt * codata::e,        10.36001 * si::volt * codata::e}},
        {17,  {12.967633 * si::volt * codata::e,       12.967633 * si::volt * codata::e}},
        {18,  {15.7596119 * si::volt * codata::e,      15.7596119 * si::volt * codata::e}},
        {19,  {4.34066373 * si::volt * codata::e,      4.34066373 * si::volt * codata::e}},
        {20,  {6.11315547 * si::volt * codata::e,      6.11315547 * si::volt * codata::e}},
        {21,  {6.56149 * si::volt * codata::e,         6.56149 * si::volt * codata::e}},
        {22,  {6.82812 * si::volt * codata::e,         6.82812 * si::volt * codata::e}},
        {23,  {6.746187 * si::volt * codata::e,        6.746187 * si::volt * codata::e}},
        {24,  {6.76651 * si::volt * codata::e,         6.76651 * si::volt * codata::e}},
        {25,  {7.434038 * si::volt * codata::e,        7.434038 * si::volt * codata::e}},
        {26,  {7.9024681 * si::volt * codata::e,       7.9024681 * si::volt * codata::e}},
        {27,  {7.88101 * si::volt * codata::e,         7.88101 * si::volt * codata::e}},
        {28,  {7.639878 * si::volt * codata::e,        7.639878 * si::volt * codata::e}},
        {29,  {7.72638 * si::volt * codata::e,         7.72638 * si::volt * codata::e}},
        {30,  {9.394197 * si::volt * codata::e,        9.394197 * si::volt * codata::e}},
        {31,  {5.999302 * si::volt * codata::e,        5.999302 * si::volt * codata::e}},
        {32,  {7.899435 * si::volt * codata::e,        7.899435 * si::volt * codata::e}},
        {33,  {9.78855 * si::volt * codata::e,         9.78855 * si::volt * codata::e}},
        {34,  {9.752392 * si::volt * codata::e,        9.752392 * si::volt * codata::e}},
        {35,  {11.81381 * si::volt * codata::e,        11.81381 * si::volt * codata::e}},
        {36,  {13.9996055 * si::volt * codata::e,      13.9996055 * si::volt * codata::e}},
        {37,  {4.1771281 * si::volt * codata::e,       4.1771281 * si::volt * codata::e}},
        {38,  {5.69486745 * si::volt * codata::e,      5.69486745 * si::volt * codata::e}},
        {39,  {6.21726 * si::volt * codata::e,         6.21726 * si::volt * codata::e}},
        {40,  {6.634126 * si::volt * codata::e,        6.634126 * si::volt * codata::e}},
        {41,  {6.75885 * si::volt * codata::e,         6.75885 * si::volt * codata::e}},
        {42,  {7.09243 * si::volt * codata::e,         7.09243 * si::volt * codata::e}},
        {43,  {7.11938 * si::volt * codata::e,         7.11938 * si::volt * codata::e}},
        {44,  {7.3605 * si::volt * codata::e,          7.3605 * si::volt * codata::e}},
        {45,  {7.4589 * si::volt * codata::e,          7.4589 * si::volt * codata::e}},
        {46,  {8.336839 * si::volt * codata::e,        8.336839 * si::volt * codata::e}},
        {47,  {7.576234 * si::volt * codata::e,        7.576234 * si::volt * codata::e}},
        {48,  {8.99382 * si::volt * codata::e,         8.99382 * si::volt * codata::e}},
        {49,  {5.7863557 * si::volt * codata::e,       5.7863557 * si::volt * codata::e}},
        {50,  {7.343918 * si::volt * codata::e,        7.343918 * si::volt * codata::e}},
        {51,  {8.608389 * si::volt * codata::e,        8.608389 * si::volt * codata::e}},
        {52,  {9.009808 * si::volt * codata::e,        9.009808 * si::volt * codata::e}},
        {53,  {10.45126 * si::volt * codata::e,        10.45126 * si::volt * codata::e}},
        {54,  {12.1298437 * si::volt * codata::e,      12.1298437 * si::volt * codata::e}},
        {55,  {3.89390572743 * si::volt * codata::e,   3.89390572743 * si::volt * codata::e}},
        {56,  {5.2116646 * si::volt * codata::e,       5.2116646 * si::volt * codata::e}},
        {57,  {5.5769 * si::volt * codata::e,          5.5769 * si::volt * codata::e}},
        {58,  {5.5386 * si::volt * codata::e,          5.5386 * si::volt * codata::e}},
        {59,  {5.4702 * si::volt * codata::e,          5.4702 * si::volt * codata::e}},
        {60,  {5.525 * si::volt * codata::e,           5.525 * si::volt * codata::e}},
        {61,  {5.58187 * si::volt * codata::e,         5.58187 * si::volt * codata::e}},
        {62,  {5.64371 * si::volt * codata::e,         5.64371 * si::volt * codata::e}},
        {63,  {5.670385 * si::volt * codata::e,        5.670385 * si::volt * codata::e}},
        {64,  {6.1498 * si::volt * codata::e,          6.1498 * si::volt * codata::e}},
        {65,  {5.8638 * si::volt * codata::e,          5.8638 * si::volt * codata::e}},
        {66,  {5.93905 * si::volt * codata::e,         5.93905 * si::volt * codata::e}},
        {67,  {6.0215 * si::volt * codata::e,          6.0215 * si::volt * codata::e}},
        {68,  {6.1077 * si::volt * codata::e,          6.1077 * si::volt * codata::e}},
        {69,  {6.18431 * si::volt * codata::e,         6.18431 * si::volt * codata::e}},
        {70,  {6.25416 * si::volt * codata::e,         6.25416 * si::volt * codata::e}},
        {71,  {5.425871 * si::volt * codata::e,        5.425871 * si::volt * codata::e}},
        {72,  {6.82507 * si::volt * codata::e,         6.82507 * si::volt * codata::e}},
        {73,  {7.549571 * si::volt * codata::e,        7.549571 * si::volt * codata::e}},
        {74,  {7.86403 * si::volt * codata::e,         7.86403 * si::volt * codata::e}},
        {75,  {7.83352 * si::volt * codata::e,         7.83352 * si::volt * codata::e}},
        {76,  {8.43823 * si::volt * codata::e,         8.43823 * si::volt * codata::e}},
        {77,  {8.96702 * si::volt * codata::e,         8.96702 * si::volt * codata::e}},
        {78,  {8.95883 * si::volt * codata::e,         8.95883 * si::volt * codata::e}},
        {79,  {9.225554 * si::volt * codata::e,        9.225554 * si::volt * codata::e}},
        {80,  {10.437504 * si::volt * codata::e,       10.437504 * si::volt * codata::e}},
        {81,  {6.1082873 * si::volt * codata::e,       6.1082873 * si::volt * codata::e}},
        {82,  {7.4166799 * si::volt * codata::e,       7.4166799 * si::volt * codata::e}},
        {83,  {7.285516 * si::volt * codata::e,        7.285516 * si::volt * codata::e}},
        {84,  {8.41807 * si::volt * codata::e,         8.41807 * si::volt * codata::e}},
        {85,  {9.31751 * si::volt * codata::e,         9.31751 * si::volt * codata::e}},
        {86,  {10.7485 * si::volt * codata::e,         10.7485 * si::volt * codata::e}},
        {87,  {4.0727411 * si::volt * codata::e,       4.0727411 * si::volt * codata::e}},
        {88,  {5.2784239 * si::volt * codata::e,       5.2784239 * si::volt * codata::e}},
        {89,  {5.380226 * si::volt * codata::e,        5.380226 * si::volt * codata::e}},
        {90,  {6.3067 * si::volt * codata::e,          6.3067 * si::volt * codata::e}},
        {91,  {5.89 * si::volt * codata::e,            5.89 * si::volt * codata::e}},
        {92,  {6.19405 * si::volt * codata::e,         6.19405 * si::volt * codata::e}},
        {93,  {6.26554 * si::volt * codata::e,         6.26554 * si::volt * codata::e}},
        {94,  {6.02576 * si::volt * codata::e,         6.02576 * si::volt * codata::e}},
        {95,  {5.97381 * si::volt * codata::e,         5.97381 * si::volt * codata::e}},
        {96,  {5.99141 * si::volt * codata::e,         5.99141 * si::volt * codata::e}},
        {97,  {6.19785 * si::volt * codata::e,         6.19785 * si::volt * codata::e}},
        {98,  {6.28166 * si::volt * codata::e,         6.28166 * si::volt * codata::e}},
        {99,  {6.36758 * si::volt * codata::e,         6.36758 * si::volt * codata::e}},
        {100, {6.5 * si::volt * codata::e,             6.5 * si::volt * codata::e}},
        {101, {6.58 * si::volt * codata::e,            6.58 * si::volt * codata::e}},
        {102, {6.62621 * si::volt * codata::e,         6.62621 * si::volt * codata::e}},
        {103, {4.96 * si::volt * codata::e,            4.96 * si::volt * codata::e}},
        {104, {6.02 * si::volt * codata::e,            6.02 * si::volt * codata::e}},
        {105, {6.8 * si::volt * codata::e,             6.8 * si::volt * codata::e}},
        {106, {7.8 * si::volt * codata::e,             7.8 * si::volt * codata::e}},
        {107, {7.7 * si::volt * codata::e,             7.7 * si::volt * codata::e}},
        {108, {7.6 * si::volt * codata::e,             7.6 * si::volt * codata::e}},
};