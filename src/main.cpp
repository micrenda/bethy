#include "bethy/Bethy.hpp"
#include "bethy/BethyTypes.hpp"
#include <boost/units/systems/si/prefixes.hpp>
#include <boost/units/systems/si/codata/physico-chemical_constants.hpp>
#include <boost/units/systems/si/codata/electromagnetic_constants.hpp>
#include <boost/units/systems/si/codata/universal_constants.hpp>
#include <boost/units/cmath.hpp>
#include <CLI/CLI.hpp>

using namespace std;
using namespace dfpe;
using namespace CLI;
using namespace boost::units;


template<typename T>
void print(const string& label, const string& symbol, T mean, T unitValue, const string unitSymbol)
{
    cout << left << setw(18) << label << left << setw(6) << symbol  << setw(12) << (double)(mean / unitValue) << " " << unitSymbol << endl;
}

int main(int argc, char **argv)
{

    CLI::App app{"Bethe-Bloch calculator"};

    std::vector<std::string> gases;
    std::vector<double> ratios;
    double   pressureArg = 101.325;

    vector<string>   bulletEnergyArg;
    vector<string>   bulletMomentumArg;

    string  bulletArg;

    app.add_option("-g, --gases", gases, "Gas molecules")->capture_default_str()->required();
    app.add_option("-x, --ratios", ratios, "Gas ratios")->capture_default_str();

    app.add_option("-p, --pressure", pressureArg, "Gas pressures [kPa]")->capture_default_str();

    auto optionE = app.add_option("--energy",   bulletEnergyArg, "Bullet kinetic energy [eV, keV, MeV, GeV]")->expected(2);
    auto optionP = app.add_option("--momentum", bulletMomentumArg, "Bullet momentum [eV/c, keV/c, MeV/c, GeV/c]")->expected(2);

    optionE->excludes(optionP);
    optionP->excludes(optionE);

    app.add_option("-b, --bullet",   bulletArg, "Bullet name or PDG Id number")->required();

    try
    {
        app.parse(argc, argv);
    }
    catch (const CLI::ParseError &e)
    {
        return app.exit(e);
    }

    using namespace boost::units::si::constants;

    // electronvolt (energy)
    QtySiEnergy     electronvolt = 1. * si::volt * codata::e;
    QtySiPressure   pressure(pressureArg * si::kilo * si::pascal);


    optional<ParticleSpecie> bulletFound = PDGTable::instance.get(bulletArg);

    if (!bulletFound.has_value())
        throw runtime_error("Unable to find the particle '" + bulletArg + "' in the PDG database. Try to use the PDG Id instead.");

    ParticleSpecie bullet = bulletFound.value();

    optional<QtySiEnergy>     bulletEnergy;
    optional<QtySiMomentum>   bulletMomentum;

    if (bulletEnergyArg.empty() && bulletMomentumArg.empty())
    {
        throw runtime_error("Must provide one of bullet energy or bullet momentum.");
    }
    else if (!bulletEnergyArg.empty())
    {
        vector<string> energyUnits = {"eV", "keV", "MeV", "GeV"};
        auto unitIt = find(energyUnits.begin(), energyUnits.end(), bulletEnergyArg[1]);
        if (unitIt != energyUnits.end())
            bulletEnergy = QtySiEnergy(stod(bulletEnergyArg[0]) * pow(10, 3 * std::distance(energyUnits.begin(), unitIt)) * electronvolt);
        else
            throw runtime_error("Unable to parse energy unit: " + bulletEnergyArg[1]);
    }
    else if (!bulletMomentumArg.empty())
    {
        vector<string> momentumUnits = {"eV/c", "keV/c", "MeV/c", "GeV/c"};
        auto           unitIt        = find(momentumUnits.begin(), momentumUnits.end(), bulletMomentumArg[1]);
        if (unitIt != momentumUnits.end())
             bulletMomentum = QtySiMomentum(stod(bulletMomentumArg[0]) * pow(10, 3 * std::distance(momentumUnits.begin(), unitIt)) * electronvolt / codata::c);
        else
            throw runtime_error("Unable to parse momentum unit: " + bulletMomentumArg[1]);
    }
    else
        assert(false);


    assert(bulletEnergy.has_value() || bulletMomentum.has_value());


    QtySiDensity density = 2.6867811e25 / (si::meter * si::meter * si::meter) * pressure /
                           QtySiPressure(101.325 * si::kilo * si::pascal);

    if (ratios.empty() && gases.size() == 1)
        ratios.push_back(100.);

    if (ratios.size() != gases.size())
    {
        cerr << "Error: please provide gases ratio" << endl;
        return 1;
    }

    double sumRatios = std::accumulate(ratios.begin(), ratios.end(), 0.);

    GasMixture gas;
    for (size_t i = 0; i < gases.size(); i++)
    {
        gas.addComponent(Molecule(gases.at(i)), ratios.at(i) / sumRatios * density);
    }

    Bethy bethloch(gas);

    QtySiEnergyPerDistance meanEnergyLoss;
    QtySiWavenumber meanInteractions;
    if (bulletEnergy.has_value())
    {
        meanEnergyLoss   = bethloch.getMeanEnergyLoss(bullet, *bulletEnergy);
        meanInteractions = bethloch.getMeanInteractions(bullet, *bulletEnergy);
    }
    else if (bulletMomentum.has_value())
    {
        meanEnergyLoss   = bethloch.getMeanEnergyLoss(bullet, *bulletMomentum);
        meanInteractions = bethloch.getMeanInteractions(bullet, *bulletMomentum);
    }
    else
        assert(false);

    for (const auto& molecule: gas.getComponents())
    {
        cout << left << setw(18) << "Molecule"      << left << setw(6) << ""   << molecule << endl;
        print("Density", "ρ", gas.getComponentMassDensity(molecule),  QtySiMassDensity(1. * si::milli * cgs::grams / (cgs::centimeter * cgs::centimeter * cgs::centimeter)), "mg/cm³");
        print("Energy exc", "I", bethloch.getMeanExcitationEnergy(molecule),  electronvolt, "eV");
        print("Energy first ion", "Eₗ", bethloch.getFirstIonizationEnergy(molecule),  electronvolt, "eV");
        print("Energy mean ion", "Wₗ", bethloch.getMeanIonizationEnergy(molecule),  electronvolt, "eV");
        cout << endl;
    }


    cout << left << setw(18) << "Mixture"      << left << setw(6) << "" << endl;
    print("Mean energy loss", "dE/dx",meanEnergyLoss,  (1000. * electronvolt) / QtySiLength(1. * cgs::centimeter), "keV/cm");
    print("Mean interactions", "N",meanInteractions,  1. / QtySiLength(1. * cgs::centimeter), "/cm");
}